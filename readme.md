# com.hcnx.mgs

[![CI Status](http://img.shields.io/travis/Guillaume MARTINEZ/com.hcnx.mgs.svg?style=flat)](https://travis-ci.org/Guillaume MARTINEZ/com.hcnx.mgs)
[![Version](https://img.shields.io/cocoapods/v/com.hcnx.mgs.svg?style=flat)](http://cocoapods.org/pods/com.hcnx.mgs)
[![License](https://img.shields.io/cocoapods/l/com.hcnx.mgs.svg?style=flat)](http://cocoapods.org/pods/com.hcnx.mgs)
[![Platform](https://img.shields.io/cocoapods/p/com.hcnx.mgs.svg?style=flat)](http://cocoapods.org/pods/com.hcnx.mgs)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

com.hcnx.mgs is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "com.hcnx.mgs"
```

## Author

Guillaume MARTINEZ, g.martinez@highconnexion.com

## License

com.hcnx.mgs is available under the MIT license. See the LICENSE file for more info.
